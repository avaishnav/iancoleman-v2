import React from "react";
import ResourceList from "./ResourceList";

const headings = [
  {
    key: "derivationPath",
    label: "Derivation Path",
  },
  {
    key: "address",
    label: "Address",
  },
  {
    key: "publicKey",
    label: "Public Key",
  },
  {
    key: "privateKey",  
    label: "Private Key",
  },
];

const getRowId = row => row.derivationPath;

const AccountsList = ({ accounts }) => {
  return <ResourceList headings={headings} rowData={accounts} getRowId={getRowId} />;
};

export default AccountsList;
