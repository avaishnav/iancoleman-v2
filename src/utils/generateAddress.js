import bs58check from 'bs58check';
import * as createHash from "create-hash";

const generateAddress = publicKey => {
  const step2 = createHash('sha256').update(publicKey).digest();
  const step3 = createHash('rmd160').update(step2).digest();
  const step4 = Buffer.allocUnsafe(21);
  step4.writeUInt8(0x6f, 0);
  step3.copy(step4, 1);
  const step9 = bs58check.encode(step4); //Steps 5-9
  return step9;
}

export default generateAddress