import wif from "wif";

const toWif = (privateKey) => {
  const privateKeyBuffer = new Buffer(privateKey, "hex");
  return wif.encode(128, privateKeyBuffer, true);
};

export default toWif;
