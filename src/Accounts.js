import React, { useEffect, useState } from 'react';
import generateAddress from './utils/generateAddress';
import toWif from './utils/toWif';
import AccountsList from './AccountsList';
import { Box, Typography } from '@material-ui/core';

const coinType = {
  'btc': 0,
  'eth': 60,
};

const Accounts = ({ root, coin, paths }) => {
  const [accounts, setAccounts] = useState([]);
  const [selectedCoin, setCoin] = useState(coin);

  useEffect(() => {
    const accounts = paths.map(account => {
      if (isNaN(account)) {
        throw new Error('Account should be a number, found ' + account);
      }
  
      const derivationPath = `m/44'/${coinType[selectedCoin]}'/0'/0/${account}`;
      const addrnode = root.derive(derivationPath);
      const address = generateAddress(addrnode._publicKey);
      const privateKey = selectedCoin === 'btc' ? toWif(addrnode.privateKey.toString ('hex')) : addrnode.privateKey.toString ('hex');
      return {
        address,
        derivationPath,
        publicKey: addrnode.publicKey.toString('hex'),
        privateKey,
        addrnode
      }
    });
    setAccounts(accounts);
  }, [selectedCoin]);

  return (
    <Box marginTop={2}>
      <Typography variant="h4">{coin.toUpperCase()} Accounts</Typography>
      <AccountsList accounts={accounts} />
    </Box>
  );
}

Accounts.defaultProps = {
  paths: [0, 1, 2, 3, 4],
};

export default Accounts;
