import React, { useState } from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import * as bip39 from "bip39";
import hdkey from 'hdkey';
import Button from "@material-ui/core/Button";
import toWif from "./utils/toWif";
import generateAddress from "./utils/generateAddress";
import Accounts from "./Accounts";


const coinType = {
  'btc': 0,
  'eth': 60,
};

function App() {
  const [mnemonic, setMnemonic] = useState(null);
  const [seed, setSeed] = useState(null);
  const [rootPrivateKey, setRootPrivateKey] = useState(null);
  const [root, setRoot] = useState(null);

  const generateSeed = async () => {
    const mnemonicString = bip39.generateMnemonic();
    setMnemonic(mnemonicString);

    const seedBytes = await bip39.mnemonicToSeed(mnemonicString);
    setSeed(seedBytes.toString("hex"));

    const root = hdkey.fromMasterSeed(seedBytes);
    setRoot(root);
    const rootPrivateKey = root.privateKey.toString('hex');
    setRootPrivateKey(rootPrivateKey);

    // const derivedNodes = deriveAccountsFor(root, 'btc', [0, 1, 2, 3, 4])
    // console.log(derivedNodes)
  };

  const deriveAccountsFor = (root, coin, accounts = []) => {
    return accounts.map(account => {
      if (isNaN(account)) {
        throw new Error('Account should be a number, found ' + account);
      }

      const derivationPath = `m/44'/${coinType[coin]}'/0'/0/${account}`;
      const addrnode = root.derive(derivationPath);
      const address = generateAddress(addrnode._publicKey);
      const privateKey = coin === 'btc' ? toWif(addrnode.privateKey.toString ('hex')) : addrnode.privateKey.toString ('hex');
      return {
        address,
        derivationPath,
        publicKey: addrnode.publicKey.toString('hex'),
        privateKey,
        addrnode
      }
    });
  }

  return (
    <Container>
      <Button variant="contained" color="primary" onClick={generateSeed}>
        Generate Seed
      </Button>
      <Divider />
      <Typography variant="h4">Mnemonic</Typography>
      <Typography variant="body1">{mnemonic && mnemonic}</Typography>
      <Divider />
      <Typography variant="h4">Mnemonic Seed</Typography>
      <Typography variant="body1">{seed && seed}</Typography>
      <Divider />
      <Typography variant="h4">Root Private Key</Typography>
      <Typography variant="body1">{rootPrivateKey && rootPrivateKey}</Typography>
      <Divider />
      {root && (
        <>
          <Accounts coin="btc" root={root} />
          <Accounts coin="eth" root={root} />
        </>
      )}
    </Container>
  );
}

export default App;
